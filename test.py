# miaekim, simple flickr scraper made with python
# get_photo에서 유저 이름 입력받으면
# url_parse(username, userparser)에서 이미지의 모든 사이즈를 접근할 수 있는 페이지 리턴
# url_parse(x, sizeparser)에서 제일 큰 사이즈의 이미지 링크 얻음

import asyncio
from html.parser import HTMLParser
import re
import urllib.request
import traceback

import aiohttp


class userPageParser(HTMLParser):
    def __init__(self):
        HTMLParser.__init__(self)
        self.link = ''
    def handle_starttag(self, tag, attrs):
        att = dict(attrs)
        if tag == "div" and 'data-photo-id' in att:
            yield 'https://www.flickr.com/photos/{username}/{photoid}/sizes/l/'.format(username=att['data-photo-owner'], photoid=att['data-photo-id'])
            # print('https://www.flickr.com/photos/{username}/{photoid}/sizes/l/'
            #       .format(username=att['data-photo-owner'], photoid=att['data-photo-id']))


class sizePageParser(HTMLParser):

    def __init__(self):
        HTMLParser.__init__(self)
        self.liststart = 0
        self.link = ''

    def handle_starttag(self, tag, attrs):
        att = dict(attrs)
        if self.liststart == 1 and 'href' in att and att['href'].startswith('/photos') != False:
            self.link = att['href']
        elif att =='div' and 'id' in att and att['id'] == 'allsizes-photo':
            self.liststart = 0
        elif 'class' in att and att['class'] == 'sizes-list':
            self.liststart = 1


userparser = userPageParser()
sizeparser = sizePageParser()

def url_parse(url, parse):
    print(url)

    if parse == userparser:
        url='http://www.flickr.com/photos/{username}'.format(username=url)

    try:
        response = yield from aiohttp.request(
            'GET',
            url
        )
    except Exception:
        traceback.print_exc()
    try:
        body = yield from response.read_and_close()
        parse.feed(body.decode("utf-8"))

    except Exception:
        traceback.print_exc()

    print('ff')
    print(parse.link)


def get_photo(username):
    for x in url_parse(username, userparser):
        url_parse(x, sizeparser)



loop = asyncio.get_event_loop()
tasks = [
        get_photo('mark_lj')
]

loop.run_until_complete(asyncio.wait(tasks))
loop.close()

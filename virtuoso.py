#!/usr/bin/env python
#
# Demonstrating use of a single query to populate a # Virtuoso Quad Store via Python. 
#

import json
import urllib.request
import urllib.parse

# HTTP URL is constructed accordingly with JSON query results format in mind.

def sparqlQuery(query, baseURL, format="application/json"):
    params = {
        "default-graph": "",
        "should-sponge": "soft",
        "query": query,
        "debug": "on",
        "timeout": "",
        "format": format,
        "save": "display",
        "fname": ""
    }
    querypart = urllib.parse.urlencode(params)
    response = urllib.request.urlopen(baseURL, querypart).read()
    return json.loads(response)

# Setting Data Source Name (DSN)
dsn = "http://dbpedia.org/resource/DBpedia"

# Virtuoso pragmas for instructing SPARQL engine to perform an HTTP GET
# using the IRI in FROM clause as Data Source URL

query = """DEFINE get:soft "replace"
SELECT DISTINCT * FROM <%s> WHERE {?s ?p ?o}""" % dsn

data = sparqlQuery(query, "http://localhost:8890/sparql/")

print("Retrived data:\n" + json.dumps(data, sort_keys=True, indent=4))

#
# End


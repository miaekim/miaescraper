#!/usr/bin/env python
#
#Demonstrating use of a single query to populate a # Virtuoso Quad Store via Python. 
#

import urllib, json

# HTTP URL is constructed accordingly with JSON query results format in mind.

def sparqlQuery(query, baseURL, format="application/json"):
    params={
        "default-graph": "",
        "should-sponge": "soft",
        "query": query,
        "debug": "on",
        "timeout": "",
        "format": format,
        "save": "display",
        "fname": ""
    }
    querypart=urllib.urlencode(params)
    response = urllib.urlopen(baseURL,querypart).read()
    return json.loads(response)

# Setting Data Source Name (DSN)
dsn="http://dbpedia.org/resource/DBpedia"

# Virtuoso pragmas for instructing SPARQL engine to perform an HTTP GET
# using the IRI in FROM clause as Data Source URL

query="""SELECT DISTINCT * FROM <%s> WHERE {?s ?p ?o}""" % dsn 


data=sparqlQuery(query, "http://localhost:8890/sparql/")
print (json.dumps(data, sort_keys=True, indent=4))

#
# End
